# standard-readme-java

Java implementation of [standard-readme](https://github.com/RichardLitt/standard-readme)
 
## Usage

### Update Gradle version

    ./gradlew wrapper --distribution-type all --gradle-version=6.3
    ./gradlew build    
