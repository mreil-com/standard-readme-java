package com.mreil.standardreadme;

import com.mreil.standardreadme.sections.SectionDefinition;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.collection.iteration.ReversiblePeekingIterator;


public class StandardReadmeParser {
    public static final Parser PARSER = Parser.builder().build();

    public void parse(Document document) {
        ReversiblePeekingIterator<Node> nodes = document.getChildren().iterator();
        StandardReadme readme = new StandardReadme();

        readme.sections
                .forEach(section -> parseSection(section, nodes));
    }

    private void parseSection(SectionDefinition section, ReversiblePeekingIterator<Node> remaining) {
        section.parseNode(remaining);
    }
}
