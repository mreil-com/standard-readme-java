package com.mreil.standardreadme;

import com.vladsch.flexmark.profile.pegdown.Extensions;
import com.vladsch.flexmark.profile.pegdown.PegdownOptionsAdapter;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.DataHolder;
import com.vladsch.flexmark.util.sequence.BasedSequence;

import java.util.Collection;

public class ReadmeRenderer {

    public Document render(Collection<Node> nodes) {
        final DataHolder options = PegdownOptionsAdapter.flexmarkOptions(
                Extensions.ALL
        );

        Document doc = new Document(options, BasedSequence.EMPTY);

        ReadmeValidator validator = new ReadmeValidator();
        validator.validate(doc);

        nodes.forEach(doc::appendChild);
        return doc;
    }
}
