package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.ast.Image;
import com.vladsch.flexmark.ast.Link;
import com.vladsch.flexmark.util.ast.Node;
import org.immutables.value.Value;

import java.util.Optional;

import static java.util.Optional.empty;

/**
 * [![Alt text](/url/img.jpg "TITLE")](/url/linktarget.html)
 */
@Value.Enclosing
public class BadgeElement extends ElementDefinition<BadgeElement.Data> {

    protected BadgeElement() {
        super();
    }

    public Node renderNode(Data data) {
        LinkElement link = new LinkElement();
        Node linkNode = link.renderNode(ImmutableLinkElement.Data.builder()
                .url(data.getTargetUrl())
                .text("")
                .build());

        ImageElement image = new ImageElement();
        Node imageNode = image.renderNode(ImmutableImageElement.Data.builder()
                .altText(data.getAltText())
                .title(data.getTitle())
                .url(data.getImageUrl())
                .build());

        linkNode.appendChild(imageNode);

        return linkNode;
    }

    public Optional<Data> parseNode(Node node) {
        if (!node.getClass().isAssignableFrom(Link.class)) {
            // TODO log
            return empty();
        }

        Node child = node.getFirstChild();
        if (!child.getClass().isAssignableFrom(Image.class)) {
            // TODO log
            return empty();
        }

        Link link = (Link) node;
        Image image = (Image) child;

        return Optional.of(ImmutableBadgeElement.Data.builder()
                .imageUrl(image.getUrl().toString())
                .targetUrl(link.getUrl().toString())
                .altText(image.getText().toStringOrNull())
                .title(Optional.ofNullable(image.getTitle().toStringOrNull()))
                .build());
    }

    @Value.Immutable
    public interface Data extends ElementData {
        String getImageUrl();

        String getTargetUrl();

        Optional<String> getAltText();

        Optional<String> getTitle();
    }
}
