package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.ast.Image;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import org.immutables.value.Value;

import java.util.Optional;

import static java.util.Optional.empty;

/**
 * ![Alt text](/url/img.jpg "Title")
 */
public class BannerElement extends ElementDefinition<BannerElement.BannerElementData> {

    protected BannerElement() {
        super();
    }

    public Node renderNode(BannerElementData data) {
        Image image = new Image();

        image.setLinkClosingMarker(BasedSequence.of(")"));
        image.setLinkOpeningMarker(BasedSequence.of("("));

        image.setUrlChars(BasedSequence.of("\"" + data.getUrl() + "\""));

        data.getTitle().ifPresent(title -> {
                    image.setTitle(BasedSequence.of(title));
                    image.setTitleOpeningMarker(BasedSequence.of("\""));
                    image.setTitleClosingMarker(BasedSequence.of("\""));
                }
        );

        data.getAltText().ifPresent(alt -> {
            image.setText(BasedSequence.of(alt));
            image.setTextOpeningMarker(BasedSequence.of("![" + alt + "]"));
        });

        return image;
    }

    public Optional<BannerElementData> parseNode(Node node) {
        if (!node.getClass().isAssignableFrom(Image.class)) {
            return empty();
        }

        Image image = (Image) node;

        return Optional.of(ImmutableBannerElementData.builder()
                .url(image.getUrl().toString())
                .altText(image.getText().toStringOrNull())
                .title(image.getTitle().toStringOrNull())
                .build());
    }

    @Value.Immutable
    public interface BannerElementData extends ElementData {
        String getUrl();

        Optional<String> getAltText();

        Optional<String> getTitle();
    }
}
