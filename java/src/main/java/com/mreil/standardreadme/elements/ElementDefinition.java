package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.util.ast.Node;

import java.util.Optional;

public abstract class ElementDefinition<T> {
    protected ElementDefinition() {
    }

    public abstract Node renderNode(T data);

    public abstract Optional<T> parseNode(Node node);
}
