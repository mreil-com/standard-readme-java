package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.ast.Heading;
import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import org.immutables.value.Value;

import java.util.Optional;

import static java.util.Optional.empty;

public class TitleElement extends ElementDefinition<String> {

    private final int level;

    public TitleElement(int level) {
        this.level = level;
    }

    public Node renderNode(String projectName) {
        Heading heading = new Heading();
        heading.setLevel(level);
        heading.setOpeningMarker(BasedSequence.of("#"));
        heading.appendChild(new Text(projectName));

        return heading;
    }

    public Optional<String> parseNode(Node node) {
        if (!node.getClass().isAssignableFrom(Heading.class)) {
            return empty();
        }

        Heading heading = (Heading) node;

        if (heading.getLevel() != level) {
            return empty();
        }

        String str = heading.getChildChars().toString();
        return Optional.of(str);
    }

    @Value.Immutable
    interface StringData extends ElementData {
        String getString();
    }
}
