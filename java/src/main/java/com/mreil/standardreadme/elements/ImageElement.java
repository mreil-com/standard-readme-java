package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.ast.Image;
import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import org.immutables.value.Value;

import java.util.Optional;

import static java.util.Optional.empty;

/**
 * ![Alt text](/url/img.jpg "Title")
 */
@Value.Enclosing
public class ImageElement extends ElementDefinition<ImageElement.Data> {

    protected ImageElement() {
        super();
    }

    public Node renderNode(Data data) {
        Image image = new Image();

        image.setLinkClosingMarker(BasedSequence.of(")"));
        image.setLinkOpeningMarker(BasedSequence.of("("));

        image.setUrlChars(BasedSequence.of(data.getUrl()));

        data.getTitle().ifPresent(title -> {
                    image.setTitle(BasedSequence.of(title));
                    image.setTitleOpeningMarker(BasedSequence.of("\""));
                    image.setTitleClosingMarker(BasedSequence.of("\""));
                }
        );

        data.getAltText().ifPresent(alt -> {
            image.appendChild(new Text(alt));
            image.setTextOpeningMarker(BasedSequence.of("!["));
            image.setTextClosingMarker(BasedSequence.of("]"));
        });

        return image;
    }

    public Optional<Data> parseNode(Node node) {
        if (!node.getClass().isAssignableFrom(Image.class)) {
            return empty();
        }

        Image image = (Image) node;

        return Optional.of(ImmutableImageElement.Data.builder()
                .url(image.getUrl().toString())
                .altText(image.getText().toStringOrNull())
                .title(image.getTitle().toStringOrNull())
                .build());
    }

    @Value.Immutable
    public interface Data extends ElementData {
        String getUrl();

        Optional<String> getAltText();

        Optional<String> getTitle();
    }
}
