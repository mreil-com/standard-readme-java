package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.ast.Link;
import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import org.immutables.value.Value;

import java.util.Optional;

import static java.util.Optional.empty;

/**
 * [description](https://www.google.com "Google")
 */
@Value.Enclosing
public class LinkElement extends ElementDefinition<LinkElement.Data> {

    protected LinkElement() {
        super();
    }

    public Node renderNode(Data data) {
        Link link = new Link();

        link.setLinkClosingMarker(BasedSequence.of(")"));
        link.setLinkOpeningMarker(BasedSequence.of("("));

        link.setUrlChars(BasedSequence.of(data.getUrl()));

        data.getTitle().ifPresent(title -> {
                    link.setTitle(BasedSequence.of(title));
                    link.setTitleOpeningMarker(BasedSequence.of("\""));
                    link.setTitleClosingMarker(BasedSequence.of("\""));
                }
        );

        data.getText().ifPresent(alt -> {
            link.appendChild(new Text(alt));
            link.setTextOpeningMarker(BasedSequence.of("["));
            link.setTextClosingMarker(BasedSequence.of("]"));
        });

        return link;
    }

    public Optional<Data> parseNode(Node node) {
        if (!node.getClass().isAssignableFrom(Link.class)) {
            return empty();
        }

        Link link = (Link) node;

        return Optional.of(ImmutableLinkElement.Data.builder()
                .url(link.getUrl().toString())
                .text(link.getText().toStringOrNull())
                .title(link.getTitle().toStringOrNull())
                .build());
    }

    @Value.Immutable
    public interface Data extends ElementData {
        String getUrl();

        Optional<String> getText();

        Optional<String> getTitle();
    }
}
