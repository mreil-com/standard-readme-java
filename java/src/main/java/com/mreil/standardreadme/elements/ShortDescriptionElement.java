package com.mreil.standardreadme.elements;

import com.vladsch.flexmark.ast.Paragraph;
import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.util.ast.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static java.util.Optional.empty;

public class ShortDescriptionElement extends ElementDefinition<String> {

    public static final int MAX_LENGTH = 120;
    private final Logger log = LoggerFactory.getLogger(ShortDescriptionElement.class);

    public Node renderNode(String shortDescription) {
        if (shortDescription.contains("\r") || shortDescription.contains("\n")) {
            throw new IllegalArgumentException("Short description cannot contain newlines");
        }

        if (shortDescription.length() > MAX_LENGTH) {
            throw new IllegalArgumentException("Short description cannot be longer than 120 characters");
        }

        Paragraph paragraph = new Paragraph();
        paragraph.appendChild(new Text(shortDescription));

        // TODO check constraints

        return paragraph;
    }

    public Optional<String> parseNode(Node node) {
        if (!node.getClass().isAssignableFrom(Paragraph.class)) {
            return empty();
        }

        Paragraph paragraph = (Paragraph) node;

        if (paragraph.getLineCount() > 1) {
            log.debug("Short description cannot have more than 1 line.");
            return empty();
        }

        if (paragraph.getChars().length() > MAX_LENGTH) {
            log.debug("Short description cannot have more than 120 characters.");
            return empty();
        }

        String str = paragraph.getChars().toString();
        return Optional.of(str);
    }
}
