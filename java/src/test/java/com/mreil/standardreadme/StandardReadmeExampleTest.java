package com.mreil.standardreadme;

import com.vladsch.flexmark.formatter.Formatter;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.IRender;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Rule;
import org.junit.Test;

public class StandardReadmeExampleTest {
    public static final Parser PARSER = Parser.builder().build();
    public static final IRender RENDERER = Formatter.builder()
            .build();

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Test
    public void testExample() {

        Document doc = PARSER.parse("[![Alt text](/url/img.jpg \"TITLE\")](/url/linktarget.html)");
        StandardReadmeExample example = new StandardReadmeExample();
        Document document = new ReadmeRenderer().render(example.getNodes());

        String render = RENDERER.render(document);
        System.err.println(render);
    }
}
