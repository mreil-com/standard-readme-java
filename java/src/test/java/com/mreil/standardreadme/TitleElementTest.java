package com.mreil.standardreadme;

import com.mreil.standardreadme.elements.TitleElement;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

public class TitleElementTest extends SectionTest {

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private TitleElement titleSection;

    @Before
    public void setupNode() {
        titleSection = new TitleElement(1);
    }

    @Test
    public void renderNode() {
        Node node = titleSection.renderNode("project-name");
        doc.appendChild(node);
        String out = RENDERER.render(doc);

        softly.assertThat(out).isEqualToIgnoringNewLines("# project-name");
    }

    @Test
    public void parseNode() {
        Document parse = PARSER.parse("# project-name");

        Optional<String> title = titleSection.parseNode(parse.getFirstChild());

        softly.assertThat(title).hasValue("project-name");
    }
}
