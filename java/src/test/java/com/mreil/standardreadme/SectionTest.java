package com.mreil.standardreadme;

import com.vladsch.flexmark.formatter.Formatter;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.profile.pegdown.Extensions;
import com.vladsch.flexmark.profile.pegdown.PegdownOptionsAdapter;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.IRender;
import com.vladsch.flexmark.util.data.DataHolder;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import org.junit.Before;

public abstract class SectionTest {
    public static final Parser PARSER = Parser.builder().build();
    public static final IRender RENDERER = Formatter.builder()
            .build();
    protected Document doc;

    @Before
    public void setupDoc() {
        final DataHolder options = PegdownOptionsAdapter.flexmarkOptions(
                Extensions.ALL
        );

        doc = new Document(options, BasedSequence.EMPTY);
    }
}
