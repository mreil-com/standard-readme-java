package com.mreil.standardreadme;

import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;

import java.util.stream.StreamSupport;

public class DebugParser {
    private final String string = "[![Alt text](/url/img.jpg \"TITLE\")](/url/linktarget.html)";
    public static final Parser PARSER = Parser.builder().build();

    public static void main(String[] args) {
        new DebugParser().parse();
    }

    public void parse() {
        Document doc = PARSER.parse(string);
        StreamSupport.stream(doc.getChildren().spliterator(), false)
                .forEach(this::print);
    }

    private void print(Node node) {
        System.err.println(node);
        node.getChildren().forEach(this::print);
    }
}
