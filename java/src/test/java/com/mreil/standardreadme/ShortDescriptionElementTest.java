package com.mreil.standardreadme;

import com.mreil.standardreadme.elements.ShortDescriptionElement;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

public class ShortDescriptionElementTest extends SectionTest {

    private static final String CONTENT = "This is a short description";

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private ShortDescriptionElement shortDescription;

    @Before
    public void setupNode() {
        shortDescription = new ShortDescriptionElement();
    }

    @Test
    public void renderNode() {
        Node node = shortDescription.renderNode(CONTENT);
        doc.appendChild(node);
        String out = RENDERER.render(doc);

        softly.assertThat(out).isEqualToIgnoringNewLines(CONTENT);
    }

    @Test
    public void renderNode_withMultipleLines_fails() {
        softly.assertThatThrownBy(() -> shortDescription.renderNode("line1\nline2"))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void parseNode() {
        Document parse = PARSER.parse(CONTENT);

        Optional<String> title = shortDescription.parseNode(parse.getFirstChild());

        softly.assertThat(title).hasValue(CONTENT);
    }

    @Test
    public void parseNode_withMultipleLines_isEmpty() {
        Document parse = PARSER.parse("line1\nlin2");

        Optional<String> title = shortDescription.parseNode(parse.getFirstChild());

        softly.assertThat(title).isEmpty();
    }
}
