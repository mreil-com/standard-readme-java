package com.mreil.standardreadme.elements;

import com.mreil.standardreadme.SectionTest;
import com.vladsch.flexmark.ast.Paragraph;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

public class LinkElementTest extends SectionTest {

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private LinkElement linkElement;

    private static final String content = "[description](https://www.google.com \"Google\")";

    @Before
    public void setupNode() {
        linkElement = new LinkElement();
    }

    @Test
    public void renderNode() {
        Node link = linkElement.renderNode(ImmutableLinkElement.Data.builder()
                .url("https://www.google.com")
                .text("description")
                .title("Google")
                .build());

        Paragraph paragraph = new Paragraph();
        paragraph.appendChild(link);

        doc.appendChild(link);
        String out = RENDERER.render(doc);

        softly.assertThat(out).isEqualToIgnoringNewLines(content);
    }

    @Test
    public void parseNodeQuoted() {
        Document doc = PARSER.parse(content);

        // first child is paragraph
        Optional<LinkElement.Data> data =
                linkElement.parseNode(doc.getFirstChild().getFirstChild());

        softly.assertThat(data).isPresent().hasValue(
                ImmutableLinkElement.Data.builder()
                        .url("https://www.google.com")
                        .text("description")
                        .title("Google")
                        .build()
        );
    }
}
