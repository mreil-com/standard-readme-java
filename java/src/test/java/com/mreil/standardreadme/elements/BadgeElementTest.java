package com.mreil.standardreadme.elements;

import com.mreil.standardreadme.SectionTest;
import com.vladsch.flexmark.ast.Paragraph;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

public class BadgeElementTest extends SectionTest {

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private BadgeElement badgeElement;

    private static final String content = "[![Alt text](/url/img.jpg \"TITLE\")](/url/linktarget.html)";

    @Before
    public void setupNode() {
        badgeElement = new BadgeElement();
    }

    @Test
    public void renderNode() {
        Node link = badgeElement.renderNode(ImmutableBadgeElement.Data.builder()
                .imageUrl("/url/img.jpg")
                .targetUrl("/url/linktarget.html")
                .altText("Alt text")
                .title("TITLE")
                .build());

        Paragraph paragraph = new Paragraph();
        paragraph.appendChild(link);

        doc.appendChild(link);
        String out = RENDERER.render(doc);

        softly.assertThat(out).isEqualToIgnoringNewLines(content);
    }

    @Test
    public void parseNode() {
        Document doc = PARSER.parse(content);

        // first child is paragraph
        Optional<BadgeElement.Data> data =
                badgeElement.parseNode(doc.getFirstChild().getFirstChild());

        softly.assertThat(data).isPresent().hasValue(
                ImmutableBadgeElement.Data.builder()
                        .targetUrl("/url/linktarget.html")
                        .imageUrl("/url/img.jpg")
                        .altText("Alt text")
                        .title("TITLE")
                        .build()
        );
    }
}
