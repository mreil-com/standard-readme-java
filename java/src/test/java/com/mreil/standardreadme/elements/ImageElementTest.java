package com.mreil.standardreadme.elements;

import com.mreil.standardreadme.SectionTest;
import com.vladsch.flexmark.ast.Paragraph;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

public class ImageElementTest extends SectionTest {

    public static final String CONTENT = "![alt Text](/url/img.jpg \"title\")";

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private ImageElement imageElement;

    @Before
    public void setupNode() {
        imageElement = new ImageElement();
    }

    @Test
    public void renderNode() {
        Node link = imageElement.renderNode(ImmutableImageElement.Data.builder()
                .url("/url/img.jpg")
                .altText("alt Text")
                .title("title")
                .build());

        Paragraph paragraph = new Paragraph();
        paragraph.appendChild(link);

        doc.appendChild(link);
        String out = RENDERER.render(doc);

        softly.assertThat(out).isEqualToIgnoringNewLines(CONTENT);
    }

    @Test
    public void parseNode() {
        Document doc = PARSER.parse(CONTENT);

        // first child is paragraph
        Optional<ImageElement.Data> data =
                imageElement.parseNode(doc.getFirstChild().getFirstChild());

        softly.assertThat(data).isPresent().hasValue(
                ImmutableImageElement.Data.builder()
                        .url("/url/img.jpg")
                        .altText("alt Text")
                        .title("title")
                        .build()
        );
    }
}
